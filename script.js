const weatherApiKey = "235c57607ba548e4bd1150957231307"
const getWeatherInfoLocation = async (location) => {
    try{
        const response = await fetch(`https://api.weatherapi.com/v1/current.json?key=${weatherApiKey}&q=${location}`,{
            mode : "cors"
        });
        const data = await response.json();
        const res = {};
        res["temp_c"] = data.current.temp_c;
        res["temp_f"] = data.current.temp_f;
        res["cityName"] = data.location.name;
        res["condition"] = data.current.condition.text;
        res["icon"] = data.current.condition.icon;
        res["countryName"] = data.location.country;
        res["time"] = data.current.last_updated;
        return res;
    }
    catch(error){
        console.log(error);
    } 
        
}
const divWeather = document.getElementById("weather-info");
const getWeatherButton = document.getElementById("getWeather");
const inputLocation = document.getElementById("location");
const locationName = document.getElementById("city-name");
const temperature = document.getElementById("temperature");
const timeShown = document.getElementById("time");
const switchCF = document.getElementById("unit-switch");
const condition = document.getElementById("condition");
const icon = document.getElementById("weather-image");


getWeatherButton.addEventListener("click", (e) => {
    e.preventDefault();
    setNewLocation(inputLocation.value)
})


const setNewLocation = async (location) => {
    divWeather.style.display = "block";
    const toDisplay = await getWeatherInfoLocation(location);
    locationName.innerText =
      toDisplay["cityName"] + ", " + toDisplay["countryName"];
    if (switchCF.checked){
        temperature.innerText = toDisplay["temp_f"] + " F";
    } else {
        temperature.innerText = toDisplay["temp_c"] + " °C";
    }
    condition.innerText = toDisplay["condition"];
    icon.src = toDisplay["icon"];
    const time=toDisplay["time"]
    var originalDate = new Date(time);
    var formattedString =
      (originalDate.getMonth() + 1).toString().padStart(2, "0") +
      "/" +
      originalDate.getDate().toString().padStart(2, "0") +
      "/" +
      originalDate.getFullYear() +
      " at " +
      originalDate.getHours().toString().padStart(2, "0") +
      ":" +
      originalDate.getMinutes().toString().padStart(2, "0");
    timeShown.innerText = "Survey done the " + formattedString; 
}